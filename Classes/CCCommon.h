//
//  CCCommon.h
//  FastNumbers
//
//  Created by CAMOBAP on 12/19/13.
//
//

#ifndef __FastNumbers__CCCommon__
#define __FastNumbers__CCCommon__

#include "cocos2d.h"
#include "CCDate.h"

#ifdef __cplusplus
#define NS_CC_EXTENSION_BEGIN           namespace cocos2d { namespace extension {
#define NS_CC_EXTENSION_END             }}
#define USING_NS_CC_EXTENSION           using namespace cocos2d::extension
#else
#define NS_CC_EXTENSION_BEGIN
#define NS_CC_EXTENSION_END
#define USING_NS_CC_EXTENSION
#endif

//
// http://stackoverflow.com/questions/2049230/convert-rgba-color-to-rgb
//
cocos2d::ccColor3B ccc3BFromccc4B(cocos2d::ccColor4B c);

cocos2d::ccColor4B zeroAlpha(cocos2d::ccColor4B c);
cocos2d::ccColor4F zeroAlpha(cocos2d::ccColor4F c);

cocos2d::ccColor4B setAlpha(cocos2d::ccColor4B c, int alpha);
cocos2d::ccColor4F setAlpha(cocos2d::ccColor4F c, float alpha);

cocos2d::ccColor4B ccc4BOverlay(cocos2d::ccColor4B c1, cocos2d::ccColor4B c2);
cocos2d::ccColor4F ccc4FOverlay(cocos2d::ccColor4F c1, cocos2d::ccColor4F c2);

void changeScaleWithoutPositionChange(cocos2d::CCNode* node, cocos2d::CCPoint newScale);

#endif /* defined(__FastNumbers__CCCommon__) */
