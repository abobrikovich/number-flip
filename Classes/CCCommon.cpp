//
//  CCCommon.cpp
//  FastNumbers
//
//  Created by CAMOBAP on 12/19/13.
//
//

#include "CCCommon.h"

using namespace cocos2d;

//
// http://stackoverflow.com/questions/2049230/convert-rgba-color-to-rgb
//
ccColor3B ccc3BFromccc4B(ccColor4B c)
{
    ccColor3B c3;
    int a = 0xff - c.a;
    
    c3.r = ((1 - a) * c.r) + a;
    c3.g = ((1 - a) * c.g) + a;
    c3.b = ((1 - a) * c.b) + a;
    
    return c3;
}

ccColor4B zeroAlpha(ccColor4B c)
{
    ccColor3B c3 = ccc3BFromccc4B(c);
    ccColor4B c4 = {c3.r, c3.g, c3.b, (GLubyte)255};
    return c4;
}

ccColor4F zeroAlpha(ccColor4F c)
{
    ccColor4F c4;
    float a = 1.f - c.a;
    
    c4.r = ((1 - a) * c.r) + a;
    c4.g = ((1 - a) * c.g) + a;
    c4.b = ((1 - a) * c.b) + a;
    c4.a = 1.f;
    
    return c4;
}


ccColor4B setAlpha(ccColor4B c, int alpha)
{
    c.a = alpha;
    return c;
}

ccColor4F setAlpha(ccColor4F c, float alpha)
{
    c.a = alpha;
    return c;
}

ccColor4F ccc4FOverlay(ccColor4F c1, ccColor4F c2)
{
    ccColor4F c1_ = zeroAlpha(c1);
    // ccColor4F c2_ = zeroAlpha(c2);
    
    ccColor4F c3 = {
        (c1_.r * (1.f - c2.a)),
        (c1_.g * (1.f - c2.a)),
        (c1_.b * (1.f - c2.a)),
        1.f
    };
    
    return c3;
}

void changeScaleWithoutPositionChange(cocos2d::CCNode* node, cocos2d::CCPoint newScale)
{
    CCPoint oldDistance = CCPointZero;
    CCPoint newDistance = CCPointZero;
    CCPoint translate = CCPointZero;
    
    newScale.x = (newScale.x == 0.f) ? node->getScaleX() : newScale.x;
    newScale.y = (newScale.x == 0.f) ? node->getScaleY() : newScale.y;
    
    if (node->getAnchorPoint().x != 0.5f)
    {
        oldDistance.x = (0.5f - node->getAnchorPoint().x) * node->getContentSize().width * node->getScaleX();
        newDistance.x = (0.5f - node->getAnchorPoint().x) * node->getContentSize().width * newScale.x;
    }
    
    if (node->getAnchorPoint().y != 0.5f)
    {
        oldDistance.y = (0.5f - node->getAnchorPoint().y) * node->getContentSize().height * node->getScaleY();
        newDistance.y = (0.5f - node->getAnchorPoint().y) * node->getContentSize().height * newScale.y;
    }
    
    translate = ccpSub(newDistance, oldDistance);
    
    node->setScaleX(newScale.x);
    node->setScaleY(newScale.y);
    
    node->setPosition(ccpSub(node->getPosition(), translate));
}
