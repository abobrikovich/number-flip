//
//  CCRandomGenerator.cpp
//  FastNumbers
//
//  Created by CAMOBAP on 12/20/13.
//
//

#include "CCRandomGenerator.h"
#include <stdlib.h>

CCRandUniqueGeneratorFromRange::CCRandUniqueGeneratorFromRange(int max) : _values(std::vector<int>())
{
    for (int i = 1; i <= max; i++)
    {
        _values.push_back(i);
    }
}

CCRandUniqueGeneratorFromRange::CCRandUniqueGeneratorFromRange(int start, int count) : _values(std::vector<int>())
{
    int finish = start + count;
    for (int i = start; i <= finish; i++)
    {
        _values.push_back(i);
    }
}

CCRandUniqueGeneratorFromRange::CCRandUniqueGeneratorFromRange(const std::vector<int> & values) : _values(values)
{
}

int CCRandUniqueGeneratorFromRange::getNextValue()
{
    int index = rand() % _values.size();

    int value = _values[index];
    _values.erase(_values.begin() + index);

    return value;
}
