//
//  PaintedFigureWithText.cpp
//  FastNumbers
//
//  Created by CAMOBAP on 12/19/13.
//
//

#include "CCPaintedFigure.h"
#include "CCCommon.h"

#include "CCGL.h"
#include "ccTypes.h"
#include "ccMacros.h"

using namespace cocos2d;

namespace cocos2d { namespace extension {
    
const ccColor4F kEmptyColor = {0.f, 0.f, 0.f, 0.f};
const char * kEmptyString = "";
   

    
bool CCPaintedFigure::initWithColor(const cocos2d::ccColor4F & color, const cocos2d::CCSize & size)
{
    return this->initWithColorAndText(color, size, "", ccc4f(0.f, 0.f, 0.f, 0.f));
}
  
bool CCPaintedFigure::initWithColorAndText(const cocos2d::ccColor4F & color, const cocos2d::CCSize & size, const char *text, const cocos2d::ccColor4F & textColor)
{
    return this->initWithColorAndText(color, size, text, textColor, 20);
}

bool CCPaintedFigure::initWithColorAndText(const cocos2d::ccColor4F & color, const cocos2d::CCSize & size, const char *text, const cocos2d::ccColor4F & textColor, int textSize)
{
    this->setPosition(CCPointZero);
    this->setContentSize(size);
    this->setAnchorPoint(ccp(0.5f, 0.5f));
    this->_mainColor = color;
    
    _label = CCLabelTTF::create(text, "Futura", _textSize = textSize);
    CC_FIX_SMOOTH_FOR_CCLABELTTF(_label)
    
    _label->setColor(ccc3BFromccc4B(ccc4BFromccc4F(textColor)));
    _label->setPosition(ccp(size.width / 2, size.height / 2));
    _label->setHorizontalAlignment(kCCTextAlignmentLeft);
    
    this->addChild(_label);
    
    return true;
}
    
void CCPaintedFigure::setText(const char * text)
{
    char buff[5];
    strcpy(buff, text);
    
    CCLabelTTF *newLabel = CCLabelTTF::create(text, "Futura", _textSize);
    CC_FIX_SMOOTH_FOR_CCLABELTTF(newLabel)
    
    newLabel->setColor(_label->getColor());
    newLabel->setPosition(_label->getPosition());
    newLabel->setHorizontalAlignment(kCCTextAlignmentLeft);
    
    this->removeChild(_label);
    this->addChild(_label = newLabel);
}

CCSolidSquareFigure* CCSolidSquareFigure::create(int halfEdge, const ccColor4F & color)
{
    return CCSolidSquareFigure::create(halfEdge, color, kEmptyString, kEmptyColor);
}

CCSolidSquareFigure* CCSolidSquareFigure::create(int halfEdge, const ccColor4F & color, const char * text, const ccColor4F & textColor)
{
    return CCSolidSquareFigure::create(halfEdge, color, text, textColor, 20);
}
    
CCSolidSquareFigure* CCSolidSquareFigure::create(int halfEdge, const ccColor4F & color, const char * text, const ccColor4F & textColor, short textSize)
{
    CCSolidSquareFigure* sprite = new CCSolidSquareFigure(halfEdge);
    
    if (sprite && sprite->initWithColorAndText(color, CCSize(2 * halfEdge, 2 * halfEdge), text, textColor, textSize))
    {
        return (CCSolidSquareFigure *)sprite->autorelease();
    }
    
    CC_SAFE_DELETE(sprite);
    return NULL;
}

void CCSolidSquareFigure::draw(void)
{
    glLineWidth(1);
    
    ccDrawColor4F(_mainColor.r, _mainColor.g, _mainColor.b, _mainColor.a);
    ccDrawSolidRect(ccp(0, 0), ccp(2 * _edge, 2 * _edge), _mainColor);
}

//******************************************************************************
    
CCSolidCircleFigure* CCSolidCircleFigure::create(int radius, const ccColor4F & color)
{
    return CCSolidCircleFigure::create(radius, color, kEmptyString, kEmptyColor);
}
    
CCSolidCircleFigure* CCSolidCircleFigure::create(int radius, const cocos2d::ccColor4F & color, const char * text, const cocos2d::ccColor4F & textColor)
{
    return CCSolidCircleFigure::create(radius, color, kEmptyString, kEmptyColor, 20);
}

CCSolidCircleFigure* CCSolidCircleFigure::create(int radius, const ccColor4F & color, const char * text, const ccColor4F & textColor, int textSize)
{
    CCSolidCircleFigure* sprite = new CCSolidCircleFigure(radius);
    
    if (sprite && sprite->initWithColorAndText(color, CCSize(radius, radius), text, textColor, textSize))
    {
        return (CCSolidCircleFigure *)sprite->autorelease();
    }
    
    CC_SAFE_DELETE(sprite);
    return NULL;
}

void CCSolidCircleFigure::draw(void)
{
    glLineWidth(1);
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    
    ccDrawColor4F(_mainColor.r, _mainColor.g, _mainColor.b, _mainColor.a);
    ccDrawSolidCircle(ccp(_radius / 2, _radius / 2), _radius, CC_DEGREES_TO_RADIANS(90), 50);
    //CCDrawNode::drawDot(ccp(0, 0), _radius, _mainColor);
}
// ******************************************************************************
    
CCCircleFigure* CCCircleFigure::create(int radius, const ccColor4F & color)
{
    return CCCircleFigure::create(radius, color, kEmptyString, kEmptyColor);
}

CCCircleFigure* CCCircleFigure::create(int radius, const ccColor4F & color, const char * text, const ccColor4F & textColor)
{
    CCCircleFigure* sprite = new CCCircleFigure(radius);
    
    if (sprite && sprite->initWithColorAndText(color, CCSize(2 * radius, 2 * radius), text, textColor))
    {
        //cocos2d::CCGLProgram *shader = new cocos2d::CCGLProgram();
        //shader->initWithVertexShaderFilename("empty.vsh", "example_Blur.fsh");
        //sprite->setShaderProgram(shader);
       
        
        return (CCCircleFigure *)sprite->autorelease();
    }
    
    CC_SAFE_DELETE(sprite);
    return NULL;
}
    
void CCCircleFigure::draw(void)
{
    CCScale9Sprite::draw();
    
    glLineWidth(5);
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    ccDrawColor4F(_mainColor.r, _mainColor.g, _mainColor.b, _mainColor.a);
    ccDrawCircle(ccp(_radius, _radius), _radius, CC_DEGREES_TO_RADIANS(90), 50, false);
}
    
// ******************************************************************************

CCSolidRectFigure* CCSolidRectFigure::create(const CCSize & size, const ccColor4F & color)
{
    return CCSolidRectFigure::create(size, color, kEmptyString, kEmptyColor);
}

CCSolidRectFigure* CCSolidRectFigure::create(const CCSize & size, const ccColor4F & color, const char * text, const ccColor4F & textColor)
{
    return CCSolidRectFigure::create(size, color, text, textColor, 20);
}
    
CCSolidRectFigure* CCSolidRectFigure::create(const CCSize & size, const ccColor4F & color, const char * text, const ccColor4F & textColor, int textSize)
{
    CCSolidRectFigure* sprite = new CCSolidRectFigure(size);
    
    if (sprite && sprite->initWithColorAndText(color, size, text, textColor, textSize))
    {
        return (CCSolidRectFigure *)sprite->autorelease();
    }
    
    CC_SAFE_DELETE(sprite);
    return NULL;
}

void CCSolidRectFigure::draw(void)
{
    glLineWidth(1);
    
    ccDrawColor4F(_mainColor.r, _mainColor.g, _mainColor.b, _mainColor.a);
    
    ccDrawSolidRect(ccp(0, 0), ccp(_size.width, _size.height), _mainColor);
}
    
// ******************************************************************************

CCRectFigure* CCRectFigure::create(const CCSize & size, const ccColor4F & color)
{
    return CCRectFigure::create(size, color, kEmptyString, kEmptyColor);
}
    
CCRectFigure* CCRectFigure::create(const CCSize & size, const ccColor4F & color, const char * text, const ccColor4F & textColor)
{
    CCRectFigure* sprite = new CCRectFigure(size);
    
    if (sprite && sprite->initWithColorAndText(color, size, text, textColor))
    {
        return (CCRectFigure *)sprite->autorelease();
    }
    
    CC_SAFE_DELETE(sprite);
    return NULL;
}

void CCRectFigure::draw(void)
{
    glLineWidth(2);
    
    ccDrawColor4F(_mainColor.r, _mainColor.g, _mainColor.b, _mainColor.a);
    ccDrawRect(ccp(0, 0), ccp(_size.width, _size.height));
}
  
// ******************************************************************************

CCRoundRectFigure* CCRoundRectFigure::create(const CCSize & size, const int radius, const ccColor4F & color)
{
    return CCRoundRectFigure::create(size, radius, color, kEmptyString, kEmptyColor);
}

CCRoundRectFigure* CCRoundRectFigure::create(const CCSize & size, const int radius, const ccColor4F & color, const char * text, const ccColor4F & textColor)
{
    CCRoundRectFigure* sprite = new CCRoundRectFigure(size, radius);
    
    if (sprite && sprite->initWithColorAndText(color, size, text, textColor))
    {
        return (CCRoundRectFigure *)sprite->autorelease();
    }
    
    CC_SAFE_DELETE(sprite);
    return NULL;
}
    
void CCRoundRectFigure::appendCubicBezier(int startPoint, cocos2d::CCPoint* vertices, cocos2d::CCPoint origin, cocos2d::CCPoint control1, cocos2d::CCPoint control2, cocos2d::CCPoint destination, int segments)
{
    //ccVertex2F vertices[segments + 1];
	
	float t = 0;
	for(int i = 0; i < segments; i++)
	{
		GLfloat x = powf(1 - t, 3) * origin.x + 3.0f * powf(1 - t, 2) * t * control1.x + 3.0f * (1 - t) * t * t * control2.x + t * t * t * destination.x;
		GLfloat y = powf(1 - t, 3) * origin.y + 3.0f * powf(1 - t, 2) * t * control1.y + 3.0f * (1 - t) * t * t * control2.y + t * t * t * destination.y;
        vertices[startPoint+i] = CCPointMake(x * CC_CONTENT_SCALE_FACTOR(), y * CC_CONTENT_SCALE_FACTOR() );
		t += 1.0f / segments;
	}
}
    
void CCRoundRectFigure::ccFillPoly(cocos2d::CCPoint *poli, int points, bool closePolygon)
{
    // Default GL states: GL_TEXTURE_2D, GL_VERTEX_ARRAY, GL_COLOR_ARRAY, GL_TEXTURE_COORD_ARRAY
	// Needed states: GL_VERTEX_ARRAY,
	// Unneeded states: GL_TEXTURE_2D, GL_TEXTURE_COORD_ARRAY, GL_COLOR_ARRAY
	glDisable(GL_TEXTURE_2D);
	//glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	//glDisableClientState(GL_COLOR_ARRAY);
    
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, poli);
	if( closePolygon )
		//	 glDrawArrays(GL_LINE_LOOP, 0, points);
		glDrawArrays(GL_TRIANGLE_FAN, 0, points);
	else
		glDrawArrays(GL_LINE_STRIP, 0, points);
	
	// restore default state
	//glEnableClientState(GL_COLOR_ARRAY);
	//glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnable(GL_TEXTURE_2D);
}

void CCRoundRectFigure::draw(void)
{
    CCPoint vertices[16];
    
    #define kappa 0.552228474
    #define cornerSegments 4
    
    vertices[0] = ccp(0,-_radius + _size.height);
    vertices[1] = ccp(0,-_radius*(1-kappa) + _size.height);
    vertices[2] = ccp(_radius*(1-kappa),0 + _size.height);
    vertices[3] = ccp(_radius,0 + _size.height);
    
    vertices[4] = ccp(_size.width-_radius,0 + _size.height);
    vertices[5] = ccp(_size.width-_radius*(1-kappa),0 + _size.height);
    vertices[6] = ccp(_size.width,-_radius*(1-kappa) + _size.height);
    vertices[7] = ccp(_size.width,-_radius + _size.height);
    
    vertices[8] = ccp(_size.width,-_size.height + _radius + _size.height);
    vertices[9] = ccp(_size.width,-_size.height + _radius*(1-kappa) + _size.height);
    vertices[10] = ccp(_size.width-_radius*(1-kappa),-_size.height + _size.height);
    vertices[11] = ccp(_size.width-_radius,-_size.height + _size.height);
    
    vertices[12] = ccp(_radius,-_size.height + _size.height);
    vertices[13] = ccp(_radius*(1-kappa),-_size.height + _size.height);
    vertices[14] = ccp(0,-_size.height+_radius*(1-kappa) + _size.height);
    vertices[15] = ccp(0,-_size.height+_radius + _size.height);
    
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    CCPoint polyVertices[4*cornerSegments+1];
    appendCubicBezier(0*cornerSegments,polyVertices,vertices[0], vertices[1], vertices[2], vertices[3], cornerSegments);
    appendCubicBezier(1*cornerSegments,polyVertices,vertices[4], vertices[5], vertices[6], vertices[7], cornerSegments);
    appendCubicBezier(2*cornerSegments,polyVertices,vertices[8], vertices[9], vertices[10], vertices[11], cornerSegments);
    appendCubicBezier(3*cornerSegments,polyVertices,vertices[12], vertices[13], vertices[14], vertices[15], cornerSegments);
    polyVertices[4*cornerSegments] = vertices[0];
    
    ccDrawColor4F(_fillColor.r, _fillColor.g, _fillColor.b, _fillColor.a);
    //ccFillPoly(polyVertices, 4*cornerSegments+1, true);
    
    ccDrawColor4F(_mainColor.r, _mainColor.g, _mainColor.b, _mainColor.a);
    glLineWidth(1);
    //glEnable(GL_LINE_SMOOTH);
    ccDrawCubicBezier(vertices[0], vertices[1], vertices[2], vertices[3], cornerSegments);
    ccDrawLine(vertices[3], vertices[4]);
    ccDrawCubicBezier(vertices[4], vertices[5], vertices[6], vertices[7], cornerSegments);
    ccDrawLine(vertices[7], vertices[8]);
    ccDrawCubicBezier(vertices[8], vertices[9], vertices[10], vertices[11], cornerSegments);
    ccDrawLine(vertices[11], vertices[12]);
    ccDrawCubicBezier(vertices[12], vertices[13], vertices[14], vertices[15], cornerSegments);
    ccDrawLine(vertices[15], vertices[0]);
    //glDisable(GL_LINE_SMOOTH);

}
    
    
}}