//
//  PersistentManager.cpp
//  FastNumbers
//
//  Created by CAMOBAP on 12/19/13.
//
//

#include "CCPersistentManager.h"

#include "cocos2d.h"
#include "LocalStorage.h"

#include <string>

namespace cocos2d { namespace extension {

static CCPersistentManager *s_SharedDirector = NULL;
    
CCPersistentManager::CCPersistentManager()
{
    std::string path = CCFileUtils::sharedFileUtils()->getWritablePath()
            + "com.takeshi.numberflip";
    
    localStorageInit(path.c_str());
}

CCPersistentManager::~CCPersistentManager()
{
    localStorageFree();
}
    
CCPersistentManager* CCPersistentManager::sharedManager(void)
{
    if (!s_SharedDirector)
    {
        s_SharedDirector = new CCPersistentManager();
    }
    
    return s_SharedDirector;
}

int CCPersistentManager::getInt(const char * key, int defaultValue)
{
    const char *value = localStorageGetItem(key);
    
    if (!value) return defaultValue;
    
    std::string str(value);
    if (str.length() == 0) return defaultValue;

    std::stringstream sstr(str);
    
    int result;
    bool code = sstr >> result;
    
    char assert[50];
    sprintf(assert, "string=%s to int=%d conversion failed", value,  result);
    
    // for some reason this assertion failed on android
    CCAssert(code, assert);
    
    return result;
}
    
float CCPersistentManager::getFloat(const char * key, float defaultValue)
{
    const char *value = localStorageGetItem(key);
    
    if (!value) return defaultValue;
    
    std::string str(value);
    if (str.length() == 0) return defaultValue;

    std::stringstream sstr(str);
    
    float result;
    bool code = sstr >> result;
    
    char assert[50];
    sprintf(assert, "string=%s to float=%f conversion failed", value, result);
    CCAssert(code, assert);
    
    return result;
}
    
const char* CCPersistentManager::getString(const char * key, const char * defaultValue)
{
    const char *value = localStorageGetItem(key);
    
    if (!value)
        return defaultValue;
    
    else
        return value;
}
    
    
CCDate CCPersistentManager::getDate(const char * key, const CCDate & defaultDate)
{
    return CCDate();
}
void CCPersistentManager::setInt(const char * key, int value)
{
    std::stringstream sstr;
    
    sstr << value;
    
    CCAssert(sstr, "int to string conversion failed");
    
    localStorageSetItem(key, sstr.str().c_str());
}
    
void CCPersistentManager::setFloat(const char * key, float value)
{
    std::stringstream sstr;
    
    sstr << value;
    
    CCAssert(sstr, "string to float conversion failed");
    
    localStorageSetItem(key, sstr.str().c_str());
}
    
void CCPersistentManager::setString(const char * key, const char * value)
{
    localStorageSetItem(key, value);
}
    
void CCPersistentManager::setDate(const char * key, const CCDate & value)
{
}

    void CCPersistentManager::remove(const char * key)
{
    localStorageRemoveItem(key);
}

}}
