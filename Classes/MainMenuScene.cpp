//
//  MainMenuScene.cpp
//  FastNumbers
//
//  Created by CAMOBAP on 12/15/13.
//
//

#include "MainMenuScene.h"
#include "GameScene.h"

#include "Constants.h"
#include "DataDirector.h"

#include "CCCommon.h"
#include "CCPaintedFigure.h"
#include "CCAmanoScoreService.h"

using namespace cocos2d;
using namespace cocos2d::extension;

ScoreBoardLayer::ScoreBoardLayer()
		: _tableRecords(NULL),
		  _totalItem(NULL),
		  _dailyItem(NULL)
{
}

ScoreBoardLayer::~ScoreBoardLayer()
{
}

bool ScoreBoardLayer::init()
{
    bool bRet = false;
	do
	{
        int themeColorIndex = DataDirector::sharedDirector()->getThemeIndex();

		//////////////////////////////////////////////////////////////////////////
		// super init first
		//////////////////////////////////////////////////////////////////////////

		CC_BREAK_IF(! CCLayerColor::initWithColor(ccc4(255, 255, 255, 255)));

		//////////////////////////////////////////////////////////////////////////
		// add your codes below...
		//////////////////////////////////////////////////////////////////////////

		CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
        CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

        int dist = 5;
        int boardWidth = 3 * (dist + kScoreTableButtonWidth);
        int boardHeight = visibleSize.height / 3;
        
        this->setContentSize(CCSizeMake(boardWidth, boardHeight));
        this->setPosition(ccp(origin.x + (visibleSize.width - boardWidth) / 2, origin.y + 0.58 * visibleSize.height));
        this->setZOrder(kOverlayZ);

        // private button
        CCPoint startCenter = ccp(kScoreTableButtonWidth / 2 + dist, boardHeight - kScoreTableButtonHeight / 2 - dist);
        
        CCSolidRectFigure *normalPrivate = CCSolidRectFigure::create(CCSize(kScoreTableButtonWidth, kScoreTableButtonHeight), ccc4FOverlay(kThemeColors4F[themeColorIndex], kButtonNormalColor4F), "Private", kButtonWhiteColor4F, 12);
        CCSolidRectFigure *pressedPrivate = CCSolidRectFigure::create(CCSize(kScoreTableButtonWidth, kScoreTableButtonHeight), ccc4FOverlay(kThemeColors4F[themeColorIndex], kButtonPressedColor4F), "Private", kButtonWhiteColor4F, 12);
        
        _privateItem = CCMenuItemSprite::create(normalPrivate, pressedPrivate, this, menu_selector(ScoreBoardLayer::manuPrivatePressed));
        
		_privateItem->setPosition(startCenter);
        _privateItem->setZOrder(kButtonZ);
        
		CC_BREAK_IF(! _privateItem);
        
        // total button
        startCenter = ccp(3 * kScoreTableButtonWidth / 2 + 2 * dist, boardHeight - kScoreTableButtonHeight / 2 - dist);
        
        CCSolidRectFigure *normalTotal = CCSolidRectFigure::create(CCSize(kScoreTableButtonWidth, kScoreTableButtonHeight), ccc4FOverlay(kThemeColors4F[themeColorIndex], kButtonNormalColor4F), "Total", kButtonWhiteColor4F, 12);
        CCSolidRectFigure *pressedTotal = CCSolidRectFigure::create(CCSize(kScoreTableButtonWidth, kScoreTableButtonHeight), ccc4FOverlay(kThemeColors4F[themeColorIndex], kButtonPressedColor4F), "Total", kButtonWhiteColor4F, 12);
        
        _totalItem = CCMenuItemSprite::create(normalTotal, pressedTotal, this, menu_selector(ScoreBoardLayer::manuTotalPressed));
        
		_totalItem->setPosition(startCenter);
        _totalItem->setZOrder(kButtonZ);
        _totalItem->selected();
        
		CC_BREAK_IF(! _totalItem);
        
        // private button
        startCenter = ccp(5 * kScoreTableButtonWidth / 2 + 3 * dist, boardHeight - kScoreTableButtonHeight / 2 - dist);
        
        CCSolidRectFigure *normalDaily = CCSolidRectFigure::create(CCSize(kScoreTableButtonWidth, kScoreTableButtonHeight), ccc4FOverlay(kThemeColors4F[themeColorIndex], kButtonNormalColor4F), "Daily", kButtonWhiteColor4F, 12);
        CCSolidRectFigure *pressedDaily = CCSolidRectFigure::create(CCSize(kScoreTableButtonWidth, kScoreTableButtonHeight), ccc4FOverlay(kThemeColors4F[themeColorIndex], kButtonPressedColor4F), "Daily", kButtonWhiteColor4F, 12);
        
        _dailyItem = CCMenuItemSprite::create(normalDaily, pressedDaily, this, menu_selector(ScoreBoardLayer::menuDailyPressed));
        
		_dailyItem->setPosition(startCenter);
        _dailyItem->setZOrder(kButtonZ);
        
		CC_BREAK_IF(! _dailyItem);
        
		CCMenu* pMenu = CCMenu::create(_privateItem, _totalItem, _dailyItem, NULL);
        // pMenu->addChild(privateItem);
        
		pMenu->setPosition(CCPointZero);
        
        CC_BREAK_IF(! pMenu);
		this->addChild(pMenu, 1);
        // Table of score
        _tableRecords = new CCArray;

        int score_distance = 10;
        for (int i = 0; i < kScoreTableSize; i++)
        {
            CCString *scoreRowString = CCString::create("");//CCString::createWithFormat("%02d: %04.3f - %s", i, row.score, row.playerName);

            CCLabelTTF *scoreRecord = CCLabelTTF::create(scoreRowString->getCString(), "Futura", 8);
            
            CC_FIX_SMOOTH_FOR_CCLABELTTF(scoreRecord)
            
            scoreRecord->setPosition(ccp(3 * kScoreTableButtonWidth / 4.f, boardHeight - 3 * dist - kScoreTableButtonHeight - score_distance * i));
            scoreRecord->setColor(ccc3BFromccc4B(kThemeColors4B[themeColorIndex]));
            scoreRecord->setHorizontalAlignment(kCCTextAlignmentLeft);

            CC_BREAK_IF(! scoreRecord);

            this->addChild(scoreRecord);
            scoreRecord->setAnchorPoint(ccp(0, 0));
            _tableRecords->addObject(scoreRecord);
        }
        
        CCAmanoScoreService::sharedService()->ranking("total", this, callfuncND_selector(ScoreBoardLayer::updateRankingTable));

		bRet = true;
	}
    while (0);

	return bRet;
}


void ScoreBoardLayer::setColor(const cocos2d::ccColor3B& color3)
{
    CCObject* pObj = NULL;
    CCARRAY_FOREACH(this->getChildren(), pObj)
    {
        ((CCSprite*)pObj)->setColor(color3);
    }
    
    ((CCPaintedFigure *)_totalItem->getNormalImage())->setMainColor(ccc4FOverlay(ccc4FFromccc3B(color3), kButtonNormalColor4F));
    ((CCPaintedFigure *)_totalItem->getSelectedImage())->setMainColor(ccc4FOverlay(ccc4FFromccc3B(color3), kButtonPressedColor4F));
    
    ((CCPaintedFigure *)_privateItem->getNormalImage())->setMainColor(ccc4FOverlay(ccc4FFromccc3B(color3), kButtonNormalColor4F));
    ((CCPaintedFigure *)_privateItem->getSelectedImage())->setMainColor(ccc4FOverlay(ccc4FFromccc3B(color3), kButtonPressedColor4F));
    
    ((CCPaintedFigure *)_dailyItem->getNormalImage())->setMainColor(ccc4FOverlay(ccc4FFromccc3B(color3), kButtonNormalColor4F));
    ((CCPaintedFigure *)_dailyItem->getSelectedImage())->setMainColor(ccc4FOverlay(ccc4FFromccc3B(color3), kButtonPressedColor4F));
}

void ScoreBoardLayer::manuTotalPressed(cocos2d::CCObject* pSender)
{
    _totalItem->selected();
    _dailyItem->unselected();
    _privateItem->unselected();

    CCAmanoScoreService::sharedService()->ranking("total", this, callfuncND_selector(ScoreBoardLayer::updateRankingTable));
}

void ScoreBoardLayer::manuPrivatePressed(cocos2d::CCObject* pSender)
{
    _privateItem->selected();
    _totalItem->unselected();
    _dailyItem->unselected();
    
    CCAmanoScoreService::sharedService()->ranking("user/total", this, callfuncND_selector(ScoreBoardLayer::updateRankingTable));
}

void ScoreBoardLayer::menuDailyPressed(cocos2d::CCObject* pSender)
{
    _totalItem->unselected();
    _privateItem->unselected();
    _dailyItem->selected();

    CCAmanoScoreService::sharedService()->ranking("daily", this, callfuncND_selector(ScoreBoardLayer::updateRankingTable));
}

void ScoreBoardLayer::updateRankingTable(CCNode* node, void *ccarray)
{
    CCArray *rows = (CCArray *)ccarray;
    
    for (int i = 0; i < kScoreTableSize; i++)
    {
        if (i < rows->count())
        {
            extension::ScoreRow* row = (extension::ScoreRow *)rows->objectAtIndex(i);
            CCString *scoreRowString = CCString::createWithFormat("%02d: %4.3f - %s", i, row->score, row->playerName);
            
            ((CCLabelTTF *)_tableRecords->objectAtIndex(i))->setString(scoreRowString->getCString());
        }
        else
        {
            ((CCLabelTTF *)_tableRecords->objectAtIndex(i))->setString("");
        }
        
    }
}

MainMenuScene::~MainMenuScene()
{
    if (_colorMenuItems)
	{
		_colorMenuItems->release();
		_colorMenuItems = NULL;
	}

    if (_colorThemeItems)
	{
		_colorThemeItems->release();
		_colorThemeItems = NULL;
	}

    //_themeColorLayer->release();

	// cpp don't need to call super dealloc
	// virtual destructor will do this
}

MainMenuScene::MainMenuScene() :
		_colorMenuItems(NULL),
		_colorThemeItems(NULL),
		_themeColorLayer(NULL),
		_scoreBoard(NULL),
		_pressedScore(NULL),
		_normalScore(NULL),
		_rankingLabel(NULL)
{
}

CCScene* MainMenuScene::scene()
{
	CCScene * scene = NULL;
	do
	{
		// 'scene' is an autorelease object
		scene = CCScene::create();
		CC_BREAK_IF(! scene);

		// 'layer' is an autorelease object
		MainMenuScene *layer = MainMenuScene::create();
		CC_BREAK_IF(! layer);

		// add layer as a child to scene
		scene->addChild(layer);
	} while (0);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool MainMenuScene::init()
{
	bool bRet = false;
	do
	{
		//////////////////////////////////////////////////////////////////////////
		// super init first
		//////////////////////////////////////////////////////////////////////////

		CC_BREAK_IF(! CCLayerColor::initWithColor(ccc4(255, 255, 255, 255)));

		//////////////////////////////////////////////////////////////////////////
		// add your codes below...
		//////////////////////////////////////////////////////////////////////////

		CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
        CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();
        
        // colors buttons
        const int distSquare = 2;
        const int sideSquare = visibleSize.width / 16;
        const int summSquareWidth1 = (kThemeColorsCount - 1) * (2 * sideSquare + distSquare);
        const int summSquareWidth2 = (kThemeColorsCount) * (2 * sideSquare + distSquare);
        
        const int themeColorIndex = DataDirector::sharedDirector()->getThemeIndex();

        _colorThemeItems = new CCArray;

        // theme color
        _themeColorLayer = CCLayerColor::create(kThemeColors4B[themeColorIndex]);
        _themeColorLayer->setColor(ccc3BFromccc4B(kThemeColors4B[themeColorIndex]));
        _themeColorLayer->setPosition(origin);
        _themeColorLayer->setContentSize(CCSizeMake(visibleSize.width, visibleSize.height / 3));
        this->addChild(_themeColorLayer);

        // start button
        CCPoint startCenter = ccp(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 5);

        CCSolidRectFigure *normalStart = CCSolidRectFigure::create(CCSize(summSquareWidth2, kStartButtonHeight), kButtonNormalColor4F, "START", kButtonWhiteColor4F, 21);
        CCSolidRectFigure *pressedStart = CCSolidRectFigure::create(CCSize(summSquareWidth2, kStartButtonHeight), kButtonPressedColor4F, "START", kButtonWhiteColor4F, 21);

        CCMenuItemSprite *startMenuItem = CCMenuItemSprite::create(normalStart, pressedStart, this, menu_selector(MainMenuScene::menuStartGameCallback));

		startMenuItem->setPosition(startCenter);
        startMenuItem->setZOrder(kButtonZ);

		CC_BREAK_IF(! startMenuItem);

        _colorMenuItems = new CCArray;

        for (int i = 0; i < kThemeColorsCount; i++)
        {
            CCPoint colorCenter = ccp(origin.x + visibleSize.width / 2 - summSquareWidth1 / 2 + i * (2 * sideSquare + distSquare),
                                      origin.y + sideSquare + visibleSize.height / 3);

            CCSolidSquareFigure *colorSprite = CCSolidSquareFigure::create(sideSquare, kThemeColors4F[i]);

            CCMenuItemSprite *colorMenuItem = CCMenuItemSprite::create(colorSprite, NULL, this, menu_selector(MainMenuScene::menuChangeColorCallback));

            colorMenuItem->setPosition(colorCenter);
            colorMenuItem->setZOrder(kButtonZ);

            CC_BREAK_IF(! colorMenuItem);

            _colorMenuItems->addObject(colorMenuItem);
        }

#if (CC_TARGET_PLATFORM != CC_PLATFORM_BLACKBERRY)
        
        float playerY = origin.y + 3 * sideSquare + visibleSize.height / 3 + distSquare + 7;
        
        _playerLabel = CCLabelTTF::create("Player", "Futura", 8);
        _playerLabel->setPosition(ccp(origin.x + 3 * sideSquare + _playerLabel->getContentSize().width / 2 - distSquare,
                                      playerY));
        _playerLabel->setColor(ccc3BFromccc4B(ccc4BFromccc4F(ccc4FOverlay(kThemeColors4F[themeColorIndex], kButtonNormalColor4F))));
        _playerLabel->setHorizontalAlignment(kCCTextAlignmentLeft);
        
        CC_FIX_SMOOTH_FOR_CCLABELTTF(_playerLabel)
        this->addChild(_playerLabel);
        
        CCSize editSize = CCSizeMake(summSquareWidth1 + sideSquare + distSquare - _playerLabel->getContentSize().width / 2, 19);
        CCRoundRectFigure *editRect = CCRoundRectFigure::create(editSize, 5, kThemeColors4F[themeColorIndex]);
        
        _colorThemeItems->addObject(editRect);
        
        _playerEdit = CCEditBox::create(editSize, editRect);
        _playerEdit->setFontColor(ccc3BFromccc4B(kThemeColors4B[themeColorIndex]));
        _playerEdit->setFontName("Futura");
        _playerEdit->setFontSize(9);
        _playerEdit->setMaxLength(20);
        _playerEdit->setPosition(ccp(origin.x + 3 * sideSquare + _playerLabel->getContentSize().width / 2 + summSquareWidth1 / 2 + distSquare, playerY));
        _playerEdit->setText(CCAmanoScoreService::sharedService()->currentUsername().c_str());
        
        this->addChild(_playerEdit);
#endif

        // score button
        CCPoint scoreCenter = ccp(origin.x + visibleSize.width / 2, origin.y + 3 * visibleSize.height / 4);

        int score_r = 46;
        //_normalScore = CCSolidCircleFigure::create(score_r, ccc4FOverlay(kThemeColors4F[themeColorIndex], kButtonNormalColor4F), "number f lip", kButtonWhiteColor4F, 12);
        //_pressedScore = CCSolidCircleFigure::create(score_r, ccc4FOverlay(kThemeColors4F[themeColorIndex], kButtonPressedColor4F), "number f lip", kButtonGrayColor4F, 12);

        _normalScore = CCSprite::create(CCString::createWithFormat("%d_normal.png", themeColorIndex)->getCString());
        _pressedScore = CCSprite::create(CCString::createWithFormat("%d_pressed.png", themeColorIndex)->getCString());
        
        CCMenuItemSprite *scoreMenuItem = CCMenuItemSprite::create(_normalScore, _pressedScore, this, menu_selector(MainMenuScene::menuShowScoreCallback));

		scoreMenuItem->setPosition(scoreCenter);
        scoreMenuItem->setZOrder(kButtonZ);
        
        float scale = 2 * score_r / _normalScore->getContentSize().width;
        changeScaleWithoutPositionChange(scoreMenuItem, ccp(scale, scale));
        
		CC_BREAK_IF(! scoreMenuItem);

        CCLabelTTF *label = CCLabelTTF::create("Number Flip", "Futura", 12);
        CC_FIX_SMOOTH_FOR_CCLABELTTF(label)
        label->setPosition(ccp(origin.x + visibleSize.width / 2, origin.y + 3 * visibleSize.height / 4));
        label->setColor(ccWHITE);
        label->setHorizontalAlignment(kCCTextAlignmentCenter);
        label->setZOrder(kButtonZ);
        this->addChild(label);
        
        int score_distance = 15;
        _rankingLabel = CCLabelTTF::create("Ranking", "Futura", 10);
        CC_FIX_SMOOTH_FOR_CCLABELTTF(_rankingLabel)
        _rankingLabel->setPosition(ccp(origin.x + visibleSize.width / 2,
                                     origin.y + 3 * visibleSize.height / 4 + score_r + score_distance));
        _rankingLabel->setColor(ccc3BFromccc4B(ccc4BFromccc4F(ccc4FOverlay(kThemeColors4F[themeColorIndex], kButtonNormalColor4F))));
        _rankingLabel->setHorizontalAlignment(kCCTextAlignmentLeft);
        // _rankingLabel->setVisible(false);
        this->addChild(_rankingLabel);

		CCMenu* pMenu = CCMenu::createWithArray(_colorMenuItems);
        pMenu->addChild(scoreMenuItem);
        pMenu->addChild(startMenuItem);

		pMenu->setPosition(CCPointZero);

        CC_BREAK_IF(! pMenu);
		this->addChild(pMenu, 1);

		this->setTouchEnabled(true);

		bRet = true;
	}
    while (0);

	return bRet;
}

void MainMenuScene::menuStartGameCallback(CCObject* pSender)
{
    // CCLOG("MainMenuScene::menuStartGameCallback");

#if (CC_TARGET_PLATFORM != CC_PLATFORM_BLACKBERRY)

    const char *str = _playerEdit->getText();
    if (strcmp(str, CCAmanoScoreService::sharedService()->currentUsername().c_str()))
    {
        CCAmanoScoreService::sharedService()->updateUsername(str);
    }

#endif

    CCScene *gameScene = GameScene::scene();
    CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(0.5, gameScene));
}

void MainMenuScene::menuShowScoreCallback(CCObject* pSender)
{
    // CCLOG("MainMenuScene::menuShowScoreCallback");
    
    // _rankingLabel->setVisible(_scoreBoard == NULL);
    
    if (_scoreBoard == NULL)
    {
        _scoreBoard = ScoreBoardLayer::create();
        this->addChild(_scoreBoard);
    }
    else
    {
        _scoreBoard->removeFromParent();
        _scoreBoard = NULL;
    }
}

void MainMenuScene::menuChangeColorCallback(CCObject* pSender)
{
    int index = _colorMenuItems->indexOfObject(pSender);

    CCAssert(index < kThemeColorsCount, "We haven't more then kThemeColorsCount themes");
    
    _playerEdit->setFontColor(ccc3BFromccc4B(kThemeColors4B[index]));
    
    _themeColorLayer->setColor(ccc3BFromccc4B(kThemeColors4B[index]));
    _rankingLabel->setColor(ccc3BFromccc4B(ccc4BFromccc4F(ccc4FOverlay(kThemeColors4F[index], kButtonNormalColor4F))));
    _playerLabel->setColor(ccc3BFromccc4B(ccc4BFromccc4F(ccc4FOverlay(kThemeColors4F[index], kButtonNormalColor4F))));
    
    if (_scoreBoard)
        _scoreBoard->setColor(ccc3BFromccc4B(kThemeColors4B[index]));
    
    _normalScore->setTexture(CCTextureCache::sharedTextureCache()->addImage(CCString::createWithFormat("%d_normal.png", index)->getCString()));
    _pressedScore->setTexture(CCTextureCache::sharedTextureCache()->addImage(CCString::createWithFormat("%d_pressed.png", index)->getCString()));

    CCObject* pObj = NULL;
    CCARRAY_FOREACH(this->_colorThemeItems, pObj)
    {
        ((CCPaintedFigure*)pObj)->setMainColor(kThemeColors4F[index]);
    }

    DataDirector::sharedDirector()->setThemeIndex(index);
}

void MainMenuScene::registerWithTouchDispatcher()
{
	// CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this,0,true);
    CCDirector::sharedDirector()->getTouchDispatcher()->addStandardDelegate(this,0);
}
