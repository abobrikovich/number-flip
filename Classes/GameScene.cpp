//
//  GameScene.cpp
//  FastNumbers
//
//  Created by CAMOBAP on 12/15/13.
//
//

#include "GameScene.h"
#include "MainMenuScene.h"

#include "Constants.h"
#include "DataDirector.h"
#include "SimpleAudioEngine.h"

#include "CCCommon.h"
#include "CCPaintedFigure.h"
#include "CCAmanoScoreService.h"

using namespace cocos2d;
using namespace cocos2d::extension;

GameStartLayer::GameStartLayer() : _label(NULL)
{
}

GameStartLayer::~GameStartLayer()
{
}

bool GameStartLayer::init()
{
    bool bRet = false;
	do
	{
		CC_BREAK_IF(! CCLayerColor::initWithColor(kButtonNormalColor4B));

		CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
        CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

        this->setPosition(ccp(origin.x, origin.y));
        this->setContentSize(visibleSize);
        this->setZOrder(kOverlayZ);

        _label = CCLabelTTF::create("", "Futura", 48);
        CC_FIX_SMOOTH_FOR_CCLABELTTF(_label)
        _label->setPosition(ccp(visibleSize.width / 2, visibleSize.height / 2));
        _label->setColor(ccc3BFromccc4B(kButtonWhiteColor4B));
        _label->setHorizontalAlignment(kCCTextAlignmentLeft);
        this->addChild(_label);

		bRet = true;
	}
    while (0);

	return bRet;
}

void GameStartLayer::setNumber(int number)
{
    CCString *str = CCString::createWithFormat("%d", number);

    _label->setString(str->getCString());

    delete str;
}


GameOverLayer::GameOverLayer() : _label(NULL), _startButton(NULL)
{
}
GameOverLayer::~GameOverLayer()
{
}

bool GameOverLayer::init()
{
    bool bRet = false;
	do
	{
		CC_BREAK_IF(! CCLayerColor::initWithColor(kButtonNormalColor4B));

		CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
        CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

        this->setPosition(ccp(origin.x, origin.y));
        this->setContentSize(visibleSize);
        this->setZOrder(kOverlayZ);

        _label = CCLabelTTF::create("Number", "Futura", 48);
        CC_FIX_SMOOTH_FOR_CCLABELTTF(_label)
        _label->setPosition(ccp(visibleSize.width / 2, visibleSize.height / 2));
        _label->setColor(ccc3BFromccc4B(kButtonWhiteColor4B));
        _label->setHorizontalAlignment(kCCTextAlignmentLeft);
        _label->setZOrder(kOverlayZ);
        this->addChild(_label);

        CCSolidRectFigure *normalStart = CCSolidRectFigure::create(CCSize(kStartButtonWidth, kStartButtonHeight), kButtonNormalColor4F, "START", kButtonWhiteColor4F);
        CCSolidRectFigure *pressedStart = CCSolidRectFigure::create(CCSize(kStartButtonWidth, kStartButtonHeight), kButtonPressedColor4F, "START", kButtonWhiteColor4F);

        _startButton = CCMenuItemSprite::create(normalStart, pressedStart, this, NULL);

		_startButton->setPosition(ccp(visibleSize.width / 2, visibleSize.height / 4));
        _startButton->setZOrder(kOverlayZ);

		CC_BREAK_IF(! _startButton);

        CCMenu* pMenu = CCMenu::create(_startButton, NULL);
		pMenu->setPosition(CCPointZero);

        CC_BREAK_IF(! pMenu);
		this->addChild(pMenu, 1);

		bRet = true;
	}
    while (0);

	return bRet;
}

void GameOverLayer::setStartHandler(CCObject* obj, SEL_MenuHandler handler)
{
    _startButton->setTarget(obj, handler);
}

void GameOverLayer::setScore(const char * string)
{
    _label->setString(string);
}

GameScene::~GameScene()
{
    if (_buttons)
	{
		_buttons->release();
		_buttons = NULL;
	}

	// cpp don't need to call super dealloc
	// virtual destructor will do this
}

GameScene::GameScene()
        :   _buttons(NULL),
            _gameOverLayer(NULL),
            _gameStartLayer(NULL),
            _timerUILabel(NULL),
            _buttonIndex(1),
            _freezTimeSec(0),
            _gameTimeMilis(0),
            _progress(0),
            _last15Numbers(NULL)
{
}

CCScene* GameScene::scene()
{
	CCScene * scene = NULL;
	do
	{
		// 'scene' is an autorelease object
		scene = CCScene::create();
		CC_BREAK_IF(! scene);

		// 'layer' is an autorelease object
		GameScene *layer = GameScene::create();
		CC_BREAK_IF(! layer);

		// add layer as a child to scene
		scene->addChild(layer);
	} while (0);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool GameScene::init()
{
	bool bRet = false;
	do
	{
		//////////////////////////////////////////////////////////////////////////
		// super init first
		//////////////////////////////////////////////////////////////////////////

        int themeColorIndex = DataDirector::sharedDirector()->getThemeIndex();
		CC_BREAK_IF(! CCLayerColor::initWithColor(kThemeColors4B[themeColorIndex]));

		//////////////////////////////////////////////////////////////////////////
		// add your codes below...
		//////////////////////////////////////////////////////////////////////////

		CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
        CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();
        
        const float distSquare = visibleSize.width / (kGameFieldSize * 12.f + kGameFieldSize + 1.f);
        const float sideSquare = (visibleSize.width - distSquare * (kGameFieldSize + 1.f)) / (1.f * kGameFieldSize);
        const float sideSquareHalf = sideSquare / 2.f;
        
        _buttons = new CCArray;

        // colors buttons
        for (int i = 0; i < kGameFieldSize; i++)
        {
            for (int j = 0; j < kGameFieldSize; j++)
            {
                CCPoint colorCenter = ccp(origin.x + sideSquareHalf + distSquare + i * (sideSquare + distSquare)
                                          ,
                                      origin.y - sideSquareHalf + (visibleSize.height - 4 * sideSquare) / 2
                                           + j * (sideSquare + distSquare));

                const char *number = "";

                CCSolidSquareFigure *colorSprite = CCSolidSquareFigure::create(sideSquareHalf, kButtonNormalColor4F, number, kButtonWhiteColor4F, 14);
                CCSolidSquareFigure *pressedSprite = CCSolidSquareFigure::create(sideSquareHalf, kButtonPressedColor4F, number, kButtonWhiteColor4F, 14);
                CCSolidSquareFigure *disabledSprite = CCSolidSquareFigure::create(sideSquareHalf, kButtonPressedColor4F, number, kButtonGrayColor4F, 14);

                CCMenuItemSprite *colorMenuItem = CCMenuItemSprite::create(colorSprite, pressedSprite, disabledSprite, this, menu_selector(GameScene::buttonPressed));

                colorMenuItem->setPosition(colorCenter);
                colorMenuItem->setZOrder(kButtonZ);
                colorMenuItem->setEnabled(false);
                
                CCPoint ancor = ccp(0.5f, 0.5f);
                CCPoint pos = ccp(sideSquareHalf, sideSquareHalf);
                
                colorSprite->setAnchorPoint(ancor);
                colorSprite->setPosition(pos);
                pressedSprite->setAnchorPoint(ancor);
                pressedSprite->setPosition(pos);
                disabledSprite->setAnchorPoint(ancor);
                disabledSprite->setPosition(pos);

                CC_BREAK_IF(! colorMenuItem);

                _buttons->addObject(colorMenuItem);
            }
        }

        int score_distance = 45;

        //CCCircleFigure *normalScore = CCCircleFigure::create(15, kButtonWhiteColor4F, "<", kButtonWhiteColor4F);
        //CCCircleFigure *pressedScore = CCCircleFigure::create(15, kButtonGrayColor4F, "<", kButtonGrayColor4F);
        CCPoint backPosition = ccp(origin.x + distSquare + sideSquareHalf,
                                   origin.y + visibleSize.height - distSquare - sideSquareHalf);
        
        CCSprite *normalScore = CCSprite::create("white_circle.png");
        CCSprite *pressedScore = CCSprite::create("gray_circle.png");
        
        CCMenuItemSprite *backMenuItem = CCMenuItemSprite::create(normalScore, pressedScore, this, menu_selector(GameScene::backPressed));

		backMenuItem->setPosition(backPosition);
        
        float scale = 3 *sideSquare / ( normalScore->getContentSize().width * 4.f);
        changeScaleWithoutPositionChange(backMenuItem, ccp(scale, scale));
        
        _nextValueLabel = CCLabelTTF::create("", "Futura", 46);
        CC_FIX_SMOOTH_FOR_CCLABELTTF(_nextValueLabel)
        _nextValueLabel->setPosition(ccp(origin.x + visibleSize.width / 2,
                                      origin.y + 3 * visibleSize.height / 4 + score_distance));
        _nextValueLabel->setColor(ccWHITE);
        _nextValueLabel->setHorizontalAlignment(kCCTextAlignmentLeft);
        this->addChild(_nextValueLabel);

        _timerUILabel = CCLabelTTF::create("0.000", "Futura", 12);
        CC_FIX_SMOOTH_FOR_CCLABELTTF(_timerUILabel)
        _timerUILabel->setPosition(ccp(origin.x + visibleSize.width - 2 * 15,
                                      origin.y + visibleSize.height - 1.5 * 15));
        _timerUILabel->setColor(ccWHITE);
        _timerUILabel->setHorizontalAlignment(kCCTextAlignmentLeft);
        this->addChild(_timerUILabel);

		CCMenu* pMenu = CCMenu::createWithArray(_buttons);
        pMenu->addChild(backMenuItem);
		pMenu->setPosition(CCPointZero);

        CC_BREAK_IF(! pMenu);
		this->addChild(pMenu, 1);

		this->setTouchEnabled(true);

		bRet = true;
	}
    while (0);

	return bRet;
}

void GameScene::onEnter()
{
    CCNode::onEnter();

    _gameStartLayer = GameStartLayer::create();
    _gameStartLayer->setNumber(kGameFreezTime);

    this->addChild(_gameStartLayer);
    this->schedule(schedule_selector(GameScene::timeBeforeStart), 1.f, kGameFreezTime - 1, 1.f);
}

void GameScene::timeBeforeStart(float time)
{
    _gameStartLayer->setNumber(kGameFreezTime - ++_freezTimeSec);

    if (_freezTimeSec == kGameFreezTime)
    {
        startGame();
    }
}

void GameScene::timerUiUpdate(float delta)
{
    _gameTimeMilis += delta;
    CCString *str = CCString::createWithFormat("%4.3f", _gameTimeMilis);
    _timerUILabel->setString(str->getCString());
    str->release();
}

void GameScene::buttonPressed(CCObject* pSender)
{
    CCMenuItemSprite *colorMenuItem = (CCMenuItemSprite *)pSender;
    int tag = colorMenuItem->getTag();

    if (_buttonIndex != tag)
    {
        return;
    }
    else
    {
        
        beginNumberFlipAnimation(colorMenuItem);
        
        colorMenuItem->setEnabled(false);

        _progress++;
    }
    
    if (_progress == kGameSpots) // all buttons opened
    {
        finishGame();
    }
    else
    {
        if (_buttonIndex < 26)
        {
            _buttonIndex++;
        }
        else
        {
            _buttonIndex = _last15Numbers->getNextValue();
        }
    
        CCString *nextValStr = CCString::createWithFormat("%d", _buttonIndex);
        _nextValueLabel->setString(nextValStr->getCString());
        nextValStr->release();
        
        // workaround for 
        CocosDenshion::SimpleAudioEngine::sharedEngine()->stopAllEffects();
        CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("tick.wav");
    }
}

void GameScene::backPressed(CCObject* pSender)
{
    CCLOG("GameScene::backPressed");

    if (_gameOverLayer)
    {
        _gameOverLayer->removeFromParent();
        // _gameOverLayer->release(); autoreleased
        _gameOverLayer = NULL;
    }

    CCScene *scene = MainMenuScene::scene();
    CCDirector::sharedDirector()->pushScene(CCTransitionFade::create(0.5, scene));
}

void GameScene::startNewGamePressed(CCObject* pSender)
{
    CCAssert(_gameOverLayer, "_gameOverLayer should be not null");
    if (_gameOverLayer)
    {
        _gameOverLayer->removeFromParent();
        _gameOverLayer = NULL;
    }
    
    _gameStartLayer = GameStartLayer::create();
    _gameStartLayer->setNumber(kGameFreezTime);
    
    this->addChild(_gameStartLayer);
    
    _freezTimeSec = 0;
    this->schedule(schedule_selector(GameScene::timeBeforeStart), 1.f, kGameFreezTime - 1, 1.f);
}

void GameScene::startGame()
{
    CCRandUniqueGeneratorFromRange *g = CCRandUniqueGeneratorFromRange
            ::getFairGenerator(kGameFieldSize * kGameFieldSize);

    CCObject* pObj = NULL;
    CCARRAY_FOREACH(_buttons, pObj)
    {
        CCMenuItemSprite *item = (CCMenuItemSprite *) pObj;

        int value = g->getNextValue();
        CCString *valueStr = CCString::createWithFormat("%d", value);
        
        char str1[5], str2[5], str3[5];
        strcpy(str1, valueStr->getCString());
        strcpy(str2, str1);
        strcpy(str3, str1);

        ((CCSolidSquareFigure *)item->getNormalImage())->setText(str1);
        ((CCSolidSquareFigure *)item->getSelectedImage())->setText(str2);
        ((CCSolidSquareFigure *)item->getDisabledImage())->setText(str3);
        ((CCSolidSquareFigure *)item->getDisabledImage())->setMainColor(kButtonPressedColor4F);
        
        item->setEnabled(true);
        item->setTag(value);
        
        valueStr->release();
    }
    delete g;

    _buttonIndex = 1;

    _gameTimeMilis = 0;
    _progress = 0;
    _timerUILabel->setString("0.000");
    _nextValueLabel->setString("1");

    if (_gameStartLayer)
    {
        _gameStartLayer->removeFromParent();
        // _gameStartLayer->release(); autoreleased
        _gameStartLayer = NULL;
    }
    
    if (_last15Numbers)
    {
        delete _last15Numbers;
    }

    _last15Numbers = CCRandUniqueGeneratorFromRange::getFairGenerator(10, 15);
    
    this->schedule(schedule_selector(GameScene::timerUiUpdate));
}

void GameScene::finishGame()
{
    this->unschedule(schedule_selector(GameScene::timerUiUpdate));
    
    _nextValueLabel->setString("");
    _timerUILabel->setString("");
    
    _gameOverLayer = GameOverLayer::create();
    _gameOverLayer->setStartHandler(this, menu_selector(GameScene::startNewGamePressed));
    
    CCAmanoScoreService::sharedService()->addNewResult(_gameTimeMilis);
    
    CCString *scoreString = CCString::createWithFormat("%4.3f", _gameTimeMilis);
    _gameOverLayer->setScore(scoreString->getCString());
    
    scoreString->release();
    
    this->addChild(_gameOverLayer);
}

void GameScene::beginNumberFlipAnimation(CCMenuItemSprite *item)
{
    CCSprite* card = (CCSprite *) item->getDisabledImage();
    
    CCOrbitCamera* camera1 = CCOrbitCamera::create(kNumberFlipDurationSec / 2.0f, 1, 0, 0.0f, 90.0f, 0.5, 0);
    CCOrbitCamera* camera2 = CCOrbitCamera::create(kNumberFlipDurationSec / 2.0f, 1, 0, 270.0f, 90.0f, 0.5, 0);
    
    CCCallFuncO* func = CCCallFuncO::create(this, callfuncO_selector(GameScene::endNumberFlipAnimation), item);
    CCActionInterval* action = (CCActionInterval*)CCSequence::create(camera1, camera2, func, NULL);
    card->runAction(action);
}

void GameScene::endNumberFlipAnimation(CCObject *itemObj)
{
    CCMenuItemSprite *item = (CCMenuItemSprite *) itemObj;
    
    CCPaintedFigure* card = (CCPaintedFigure *) item->getDisabledImage();
    card->setMainColor(kButtonDisabledColor4F);
}

void GameScene::registerWithTouchDispatcher()
{
	// CCTouchDispatcher::sharedDispatcher()->addTargetedDelegate(this,0,true);
    CCDirector::sharedDirector()->getTouchDispatcher()->addStandardDelegate(this,0);
}
