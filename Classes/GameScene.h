//
//  GameScene.h
//  FastNumbers
//
//  Created by CAMOBAP on 12/15/13.
//
//

#ifndef __FastNumbers__GameScene__
#define __FastNumbers__GameScene__

#include "cocos2d.h"

#include "CCRandomGenerator.h"

class GameStartLayer : public cocos2d::CCLayerColor
{
public:
	GameStartLayer();
	virtual ~GameStartLayer();

	virtual bool init();
    virtual void setNumber(int number);

    CREATE_FUNC(GameStartLayer);

private:
    cocos2d::CCLabelTTF *_label;
};

class GameOverLayer : public cocos2d::CCLayerColor
{
public:
	GameOverLayer();
	virtual ~GameOverLayer();

	virtual bool init();
    void setStartHandler(cocos2d::CCObject* obj, cocos2d::SEL_MenuHandler handler);
    void setScore(const char *string);

    CREATE_FUNC(GameOverLayer);

private:
    cocos2d::CCLabelTTF *_label;
    cocos2d::CCMenuItemSprite *_startButton;
};

class GameScene : public cocos2d::CCLayerColor
{
public:
	GameScene();
	virtual ~GameScene();

	// Here's a difference. Method 'init' in cocos2d-x returns bool,
    // instead of returning 'id' in cocos2d-iphone
	virtual bool init();
	virtual void onEnter();

	// there's no 'id' in cpp, so we recommand to return the exactly class pointer
	static cocos2d::CCScene* scene();

	virtual void buttonPressed(cocos2d::CCObject* pSender);
    virtual void backPressed(cocos2d::CCObject* pSender);
    virtual void startNewGamePressed(cocos2d::CCObject* pSender);

    virtual void timeBeforeStart(float time);
    virtual void timerUiUpdate(float time);

	// implement the "static node()" method manually
	CREATE_FUNC(GameScene);

protected:

    void startGame();
    void finishGame();

	void registerWithTouchDispatcher();
    
    void beginNumberFlipAnimation(cocos2d::CCMenuItemSprite *item);
    void endNumberFlipAnimation(cocos2d::CCObject *obj);
    
    GameOverLayer* _gameOverLayer;
    GameStartLayer* _gameStartLayer;

    cocos2d::CCLabelTTF* _timerUILabel;
    cocos2d::CCLabelTTF* _nextValueLabel;

	cocos2d::CCArray *_buttons;
    int _buttonIndex;
    int _progress;
    
    int _freezTimeSec;
    float _gameTimeMilis;
    
    CCRandUniqueGeneratorFromRange *_last15Numbers;
};

#endif /* defined(__FastNumbers__GameScene__) */
