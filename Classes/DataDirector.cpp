//
//  DataDirector.cpp
//  FastNumbers
//
//  Created by CAMOBAP on 12/20/13.
//
//

#include "DataDirector.h"

#include "cocos2d.h"
#include "CCPersistentManager.h"
#include "Constants.h"

#include <string>

using namespace cocos2d;
using namespace cocos2d::extension;

static DataDirector *s_SharedDirector = NULL;

const char THEME_INDEX[] = "THEME_INDEX";

DataDirector::DataDirector(void)
		: _themeIndex(-1)
{
    CCLog("DataDirector initialization done");
}

DataDirector::~DataDirector()
{
}

DataDirector* DataDirector::sharedDirector(void)
{
    if (!s_SharedDirector)
    {
        s_SharedDirector = new DataDirector();
    }

    return s_SharedDirector;
}

int DataDirector::getThemeIndex()
{
    if (_themeIndex == -1)
    {
        _themeIndex = CCPersistentManager::sharedManager()->getInt(THEME_INDEX, 0);
    }

    return _themeIndex;
}

void DataDirector::setThemeIndex(int index)
{
    CCPersistentManager::sharedManager()->setInt(THEME_INDEX, _themeIndex = index);
}

