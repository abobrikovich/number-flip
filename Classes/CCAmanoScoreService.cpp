//
//  CCAmanoScoreService.cpp
//  FastNumbers
//
//  Created by CAMOBAP on 1/18/14.
//
//

#include "CCAmanoScoreService.h"
#include "CCPersistentManager.h"
#include "CCDate.h"

// deadline deadline (((
#include "MainMenuScene.h"

#include <openssl/sha.h>

#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>

const std::string DEV_SERVICE_URL = "http://appscore.sb.hautecouture.jp";
const std::string PROD_SERVICE_URL = "http://appscore.hautecouture.jp";

const int APP_ID = 2;
const char APP_KEY[] = "63624b075719a5c53388b00dff787920";

const char USER_ID_PKEY[] = "USER_ID_PKEY";
const char USER_KEY_PKEY[] = "USER_KEY_PKEY";
const char USER_NAME_PKEY[] = "USER_NAME_PKEY";

NS_CC_EXTENSION_BEGIN

static CCAmanoScoreService* _service = NULL;

CCAmanoScoreService::CCAmanoScoreService()
{
}

CCAmanoScoreService::~CCAmanoScoreService()
{
}

CCAmanoScoreService* CCAmanoScoreService::sharedService()
{
    if (!_service)
    {
        _service = new CCAmanoScoreService;
    }
    
    return _service;
}

void CCAmanoScoreService::init()
{
    if (CCPersistentManager::sharedManager()->getInt(USER_ID_PKEY, -1) == -1) {
        createUser();
    } else {
        _user_id = CCPersistentManager::sharedManager()->getInt(USER_ID_PKEY, 0);
        _user_key = CCPersistentManager::sharedManager()->getString(USER_KEY_PKEY, "");
        _user_name = CCPersistentManager::sharedManager()->getString(USER_NAME_PKEY, "no name");
        
        CCLOG("User already created %s [%d/%s]", _user_name.c_str(), _user_id, _user_key.c_str());
        
        // deadline deadline (((
    }
}

std::string CCAmanoScoreService::currentUsername()
{
    return this->_user_name;
}

void CCAmanoScoreService::updateUsername(const char *playerName)
{
    // copy
    char *player_name = new char[strlen(playerName)];
    strcpy(player_name, playerName);
    
    std::stringstream url;
    
    url << DEV_SERVICE_URL << "/users/" << _user_id;
    
    CCHttpRequest* request = new CCHttpRequest();
    request->setUrl(url.str().c_str());
    request->setRequestType(cocos2d::extension::CCHttpRequest::kHttpPut);
    request->setResponseCallback(this, httpresponse_selector(CCAmanoScoreService::onHttpRequestCompleted));
    request->setUserData(player_name);
    
    CCDate date;
    char dateStr[40];
    date.to_str(dateStr, 40);
    
    char hash[65];
    updateUserLevelHash(dateStr, hash);
    
    date.to_url_encoded_str(dateStr, 40);
    
    char postData[250];
    sprintf(postData, "user_id=%d&app_id=%d&timestamp=%s&hash=%s&name=%s", _user_id, APP_ID, dateStr, hash, playerName);
    
    request->setRequestData(postData, strlen(postData));
    
    request->setTag("uname");
    cocos2d::extension::CCHttpClient::getInstance()->send(request);
    
    request->release();
}

void CCAmanoScoreService::addNewResult(const float score)
{
    const char *url = (DEV_SERVICE_URL + "/scores").c_str();
    
    CCHttpRequest* request = new CCHttpRequest();
    request->setUrl(url);
    request->setRequestType(cocos2d::extension::CCHttpRequest::kHttpPost);
    request->setResponseCallback(this, httpresponse_selector(CCAmanoScoreService::onHttpRequestCompleted));
    
    CCDate date;
    char dateStr[40];
    date.to_str(dateStr, 40);
    
    char hash[65];
    updateUserLevelHash(dateStr, hash);
    
    date.to_url_encoded_str(dateStr, 40);
    
    char postData[250];
    sprintf(postData, "user_id=%d&app_id=%d&timestamp=%s&hash=%s&score=%4.3f",  _user_id, APP_ID, dateStr, hash, score);
    
    request->setRequestData(postData, strlen(postData));
    
    request->setTag("cscore");
    cocos2d::extension::CCHttpClient::getInstance()->send(request);
    
    request->release();
}

void CCAmanoScoreService::ranking(const char *type, CCNode *target, SEL_CallFuncND func, int count, int offset)
{
    const char *url = (DEV_SERVICE_URL + "/ranking/" + type).c_str();
    
    CCDate date;
    char dateStr[40];
    date.to_str(dateStr, 40);
    
    char hash[65];
    updateAppLevelHash(dateStr, hash);
    
    date.to_url_encoded_str(dateStr, 40);
    
    char postData[350];
    sprintf(postData, "%s?app_id=%d&timestamp=%s&hash=%s&count=%d&offset=%d&asc=1", url, APP_ID, dateStr, hash, count, offset);
    
    CCHttpRequest* request = new CCHttpRequest();
    request->setUrl(postData);
    request->setRequestType(cocos2d::extension::CCHttpRequest::kHttpGet);
    request->setResponseCallback(this, httpresponse_selector(CCAmanoScoreService::onHttpRequestCompleted));
    
    _callback_target = target;
    _score_callback = func;
    
    request->setTag("granking");
    cocos2d::extension::CCHttpClient::getInstance()->send(request);
    
    request->release();
}

void CCAmanoScoreService::createUser()
{
    const char *url = (DEV_SERVICE_URL + "/users").c_str();
    
    CCHttpRequest* request = new CCHttpRequest();
    request->setUrl(url);
    request->setRequestType(cocos2d::extension::CCHttpRequest::kHttpPost);
    request->setResponseCallback(this, httpresponse_selector(CCAmanoScoreService::onHttpRequestCompleted));
    
    CCDate date;
    char dateStr[40];
    date.to_str(dateStr, 40);
    
    char hash[65];
    updateAppLevelHash(dateStr, hash);
    
    date.to_url_encoded_str(dateStr, 40);
    
    char postData[150];
    sprintf(postData, "app_id=%d&timestamp=%s&hash=%s", APP_ID, dateStr, hash);
    
    request->setRequestData(postData, strlen(postData));
    
    request->setTag("cuser");
    cocos2d::extension::CCHttpClient::getInstance()->send(request);
    
    request->release();
}

void CCAmanoScoreService::onHttpRequestCompleted(CCHttpClient *sender, CCHttpResponse *response)
{
    std::vector<char> *buffer = response->getResponseData();
 
    if (buffer->size() == 0) return;
    
    int brackets = 1;
    int index = 1;
    
    while (brackets != 0)
    {
        if (buffer->operator[](index) == '{') brackets++;
        if (buffer->operator[](index) == '}') brackets--;
        
        index++;
    }
    
    buffer->operator[](index) = '\0';
    
    const char *cstr = &(buffer->operator[](0));
    CCLOG("response [%d] %s", response->getResponseCode(), cstr);
    
    
    if (response->getResponseCode() == 200 || response->getResponseCode() == 201 || response->getResponseCode() == 202)
    {
        rapidjson::Document d;
        d.Parse<0>(&(response->getResponseData()->operator[](0)));
    
        bool success = d["status"].GetInt() == 1;
    
        const char *tag = response->getHttpRequest()->getTag();
    
        if (success && !strcmp("cuser", tag))
        {
            int id = d["user"]["id"].GetInt();
            const char *key = d["user"]["key"].GetString();
            const char *name = d["user"]["name"].GetString();
            
            CCPersistentManager::sharedManager()->setInt(USER_ID_PKEY, id);
            CCPersistentManager::sharedManager()->setString(USER_KEY_PKEY, key);
            CCPersistentManager::sharedManager()->setString(USER_NAME_PKEY, name);
            
            _user_id = id;
            _user_key = key;
            _user_name = name;
            
            CCLOG("User created %d/%s", id, key);
            
            // deadline deadline ((((
            //CCDirector::sharedDirector()->runWithScene(MainMenuScene::scene());
        }
        else if (success && !strcmp("uname", tag))
        {
            char * playerName = (char *)response->getHttpRequest()->getUserData();
            _user_name = playerName;
            CCPersistentManager::sharedManager()->setString(USER_NAME_PKEY, playerName);
        }
        else if (success && !strcmp("cscore", tag))
        {
        }
        else if (!strcmp("granking", tag))
        {
            CCArray rows;
            
            if (success)
            {
                for (int i = 0; i < d["ranking"].Capacity(); i++)
                {
                    ScoreRow *row = new ScoreRow(d["ranking"][i]["name"].GetString(), d["ranking"][i]["score"].GetDouble());
                    rows.addObject(row);
                }
            }
            
            if (_score_callback && _callback_target)
            {
                (_callback_target->*_score_callback)(_callback_target, &rows);
            }
            
            
        }
    }
    //else
    //{
    //    std::vector<char> *buffer = response->getResponseData();
    //    const char *cstr = &(buffer->operator[](0));
    //    CCLOG("response [%d] %s", response->getResponseCode(), cstr);
    //}
    
}

void CCAmanoScoreService::updateUserLevelHash(const char *date, char hashOut[65])
{
    std::stringstream ss;
    ss << _user_id << APP_ID << date << _user_key << APP_KEY;
    
    const char *input = ss.str().c_str();
    
    // http://stackoverflow.com/questions/13784434/gcc-use-openssls-sha256-functions
    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    SHA256_Update(&sha256, input, strlen(input));
    SHA256_Final(hash, &sha256);
    int i = 0;
    for(i = 0; i < SHA256_DIGEST_LENGTH; i++)
    {
        sprintf(hashOut + (i * 2), "%02x", hash[i]);
    }
    hashOut[64] = 0;
}

void CCAmanoScoreService::updateAppLevelHash(const char *date, char hashOut[65])
{
    std::stringstream ss;
    ss << APP_ID << date << APP_KEY;
    
    const char *input = ss.str().c_str();
    
    // http://stackoverflow.com/questions/13784434/gcc-use-openssls-sha256-functions
    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    SHA256_Update(&sha256, input, strlen(input));
    SHA256_Final(hash, &sha256);
    int i = 0;
    for(i = 0; i < SHA256_DIGEST_LENGTH; i++)
    {
        sprintf(hashOut + (i * 2), "%02x", hash[i]);
    }
    hashOut[64] = 0;
}

NS_CC_EXTENSION_END
