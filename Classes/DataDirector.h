//
//  DataDirector.h
//  FastNumbers
//
//  Created by CAMOBAP on 12/20/13.
//
//

#ifndef __FastNumbers__DataDirector__
#define __FastNumbers__DataDirector__

#include "cocos2d.h"
#include "CCDate.h"

struct ScoreRow
{
    const char * playerName;
    float score;
    CCDate date;
};

const ScoreRow EMPTY_SCORE = { "EMPTY", 0.f, CCDate() };

class DataDirector : public cocos2d::CCObject
{
public:
    virtual ~DataDirector();
    static DataDirector* sharedDirector(void);
    
    int getThemeIndex();
    void setThemeIndex(int index);

private:
    DataDirector(void);
    
    int _themeIndex;
    
};

#endif /* defined(__FastNumbers__DataDirector__) */
