//
//  PersistentManager.h
//  FastNumbers
//
//  Created by CAMOBAP on 12/19/13.
//
//

#ifndef __FastNumbers__PersistentManager__
#define __FastNumbers__PersistentManager__

#include "cocos2d.h"
#include "CCDate.h"

namespace cocos2d { namespace extension {
 
class CCPersistentManager : public CCObject
{
public:
    virtual ~CCPersistentManager();
    
    static CCPersistentManager* sharedManager(void);
    
    int getInt(const char * key, const int defaultValue);
    float getFloat(const char * key, const float defaultValue);
    const char* getString(const char * key, const char * defaultValue);
    CCDate getDate(const char * key, const CCDate & defaultDate);
    
    void setInt(const char * key, const int value);
    void setFloat(const char * key, const float value);
    void setString(const char * key, const char * value);
    void setDate(const char * key, const CCDate & value);
    
    void remove(const char * key);
    
private:
    CCPersistentManager(void);
};
    
}}

#endif /* defined(__FastNumbers__PersistentManager__) */
