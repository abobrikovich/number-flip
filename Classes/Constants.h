//
//  ThemeConstants.h
//  FastNumbers
//
//  Created by CAMOBAP on 12/17/13.
//
//

#ifndef FastNumbers_ThemeConstants_h
#define FastNumbers_ThemeConstants_h

#include "cocos2d.h"

using namespace cocos2d;

const ccColor4B kGreenTheme4B = {1, 196, 150, 255};
const ccColor4B kBlueTheme4B = {38, 145, 227, 255};
const ccColor4B kPurpleTheme4B = {146, 73, 176, 255};
const ccColor4B kRedTheme4B = {226, 64, 54, 255};
const ccColor4B kOrangeTheme4B = {226, 117, 31, 255};

const ccColor4F kGreenTheme4F = ccc4FFromccc4B(kGreenTheme4B);
const ccColor4F kBlueTheme4F = ccc4FFromccc4B(kBlueTheme4B);
const ccColor4F kPurpleTheme4F = ccc4FFromccc4B(kPurpleTheme4B);
const ccColor4F kRedTheme4F = ccc4FFromccc4B(kRedTheme4B);
const ccColor4F kOrangeTheme4F = ccc4FFromccc4B(kOrangeTheme4B);

const short kThemeColorsCount = 5;
const ccColor4B kThemeColors4B[] = {kGreenTheme4B, kBlueTheme4B, kPurpleTheme4B, kRedTheme4B, kOrangeTheme4B};
const ccColor4F kThemeColors4F[] = {kGreenTheme4F, kBlueTheme4F, kPurpleTheme4F, kRedTheme4F, kOrangeTheme4F};

const ccColor4F kButtonNormalColor4F = ccc4f(0.f, 0.f, 0.f, .2f);
const ccColor4F kButtonPressedColor4F = ccc4f(0.f, 0.f, 0.f, .4f);
const ccColor4F kButtonDisabledColor4F = ccc4f(0.f, 0.f, 0.f, .0f);
const ccColor4F kButtonWhiteColor4F = ccc4f(1.f, 1.f, 1.f, 1.f);
const ccColor4F kButtonGrayColor4F = ccc4f(.7f, .7f, .7f, 1.f);

const ccColor4B kButtonNormalColor4B = ccc4BFromccc4F(kButtonNormalColor4F);
const ccColor4B kButtonPressedColor4B = ccc4BFromccc4F(kButtonPressedColor4F);
const ccColor4B kButtonDisabledColor4B = ccc4BFromccc4F(kButtonDisabledColor4F);
const ccColor4B kButtonWhiteColor4B = ccc4BFromccc4F(kButtonWhiteColor4F);

const int kButtonZ = 5;
const int kOverlayZ = 10;

const int kGameFieldSize = 5;
const int kGameSpots = kGameFieldSize * kGameFieldSize;
const int kGameNumberWidth = 36;
const int kGameNumberHalfWidth = kGameNumberWidth / 2;

const int kGameNumberDistance = 2;
const int kGameFieldWidth = kGameFieldSize * (kGameNumberWidth + kGameNumberDistance) - kGameNumberDistance;

const float kNumberFlipDurationSec = .15f;

const int kStartButtonWidth = 100;
const int kStartButtonHeight = 35;

const int kScoreTableButtonWidth = 45;
const int kScoreTableButtonHeight = 20;
const int kScoreTableSize = 9;

const int kGameFreezTime = 3;

#endif
