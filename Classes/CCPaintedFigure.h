//
//  PaintedFigureWithText.h
//  FastNumbers
//
//  Created by CAMOBAP on 12/19/13.
//
//

#ifndef __FastNumbers__PaintedFigure__
#define __FastNumbers__PaintedFigure__

#include "cocos2d.h"
#include "CCScale9Sprite.h"

//
// http://www.cocos2d-x.org/forums/6/topics/32206?r=42665#message-42665 - fix blury text
//
#define CC_FIX_SMOOTH_FOR_CCLABELTTF(_cclabelttf) \
    _cclabelttf->setFontSize(2 * _cclabelttf->getFontSize()); \
    _cclabelttf->setScale(0.5f);

namespace cocos2d { namespace extension {
 
    
class CCPaintedFigure : public CCScale9Sprite
{
public:
    CCPaintedFigure() { }
    virtual ~CCPaintedFigure() { }
    
    CC_SYNTHESIZE(cocos2d::ccColor4F, _mainColor, MainColor)
    void setText(const char * text);
    
protected:
    bool initWithColor(const cocos2d::ccColor4F & color, const cocos2d::CCSize & size);
    bool initWithColorAndText(const cocos2d::ccColor4F & color, const cocos2d::CCSize & size, const char * text, const cocos2d::ccColor4F & textColor);
    bool initWithColorAndText(const cocos2d::ccColor4F & color, const cocos2d::CCSize & size, const char * text, const cocos2d::ccColor4F & textColor, int textSize);
    
    CCLabelTTF *_label;
    int _textSize;
};
    
/**
 * Simple circle
 */
class CCSolidCircleFigure : public CCPaintedFigure
{
public:
    virtual ~CCSolidCircleFigure() { }
    virtual void draw(void);
    
    static CCSolidCircleFigure* create(int radius, const cocos2d::ccColor4F & color);
    static CCSolidCircleFigure* create(int radius, const cocos2d::ccColor4F & color, const char * text, const cocos2d::ccColor4F & textColor);
    static CCSolidCircleFigure* create(int radius, const cocos2d::ccColor4F & color, const char * text, const cocos2d::ccColor4F & textColor, int textSize);
    
private:
    CCSolidCircleFigure(int radius) : _radius(radius) {}
    
    int _radius;
};
    
class CCCircleFigure : public CCPaintedFigure
{
public:
    virtual ~CCCircleFigure() { }
    virtual void draw(void);
    
    static CCCircleFigure* create(int radius, const cocos2d::ccColor4F & color);
    static CCCircleFigure* create(int radius, const cocos2d::ccColor4F & color, const char * text, const cocos2d::ccColor4F & textColor);
    
private:
    CCCircleFigure(int radius) : _radius(radius) {}
    
    int _radius;
};
    
/**
 *
 */
class CCSolidSquareFigure : public CCPaintedFigure
{
public:
    virtual ~CCSolidSquareFigure() { }
    virtual void draw(void);
    
    static CCSolidSquareFigure* create(int edge, const cocos2d::ccColor4F & color);
    static CCSolidSquareFigure* create(int edge, const cocos2d::ccColor4F & color, const char * text, const cocos2d::ccColor4F & textColor);
    static CCSolidSquareFigure* create(int edge, const cocos2d::ccColor4F & color, const char * text, const cocos2d::ccColor4F & textColor, short textSize);
private:
    CCSolidSquareFigure(int edge) : _edge(edge) {}
    
    int _edge;
};    

class CCSolidRectFigure : public CCPaintedFigure
{
public:
    virtual ~CCSolidRectFigure() { }
    virtual void draw(void);
    
    static CCSolidRectFigure* create(const cocos2d::CCSize & size, const cocos2d::ccColor4F & color);
    static CCSolidRectFigure* create(const cocos2d::CCSize & size, const cocos2d::ccColor4F & color, const char * text, const cocos2d::ccColor4F & textColor);
    static CCSolidRectFigure* create(const cocos2d::CCSize & size, const cocos2d::ccColor4F & color, const char * text, const cocos2d::ccColor4F & textColor, int textSize);
    
private:
    CCSolidRectFigure(const cocos2d::CCSize & size) : _size(size) {}
    
    cocos2d::CCSize _size;
};
    
class CCRectFigure : public CCPaintedFigure
{
public:
    virtual ~CCRectFigure() { }
    virtual void draw(void);
    
    static CCRectFigure* create(const cocos2d::CCSize & size, const cocos2d::ccColor4F & color);
    static CCRectFigure* create(const cocos2d::CCSize & size, const cocos2d::ccColor4F & color, const char * text, const cocos2d::ccColor4F & textColor);
    
private:
    CCRectFigure(const cocos2d::CCSize & size) : _size(size) {}
    
    cocos2d::CCSize _size;
};

class CCRoundRectFigure : public CCPaintedFigure
{
public:
    virtual ~CCRoundRectFigure() { }
    virtual void draw(void);
    
    static CCRoundRectFigure* create(const cocos2d::CCSize & size, const int radius, const cocos2d::ccColor4F & color);
    static CCRoundRectFigure* create(const cocos2d::CCSize & size, const int radius, const cocos2d::ccColor4F & color, const char * text, const cocos2d::ccColor4F & textColor);
    
private:
    CCRoundRectFigure(const cocos2d::CCSize & size, const int radius) : _size(size), _radius(radius), _fillColor(cocos2d::ccc4f(1.f, 1.f, 1.f, 1.f)) {}
    
    void appendCubicBezier(int startPoint, cocos2d::CCPoint* vertices, cocos2d::CCPoint origin, cocos2d::CCPoint control1, cocos2d::CCPoint control2, cocos2d::CCPoint destination, int segments);
    void ccFillPoly(cocos2d::CCPoint *poli, int points, bool closePolygon);

    cocos2d::CCSize _size;
    int _radius;
    
    cocos2d::ccColor4F _fillColor;
};
}}

#endif /* defined(__FastNumbers__PaintedFigureWithText__) */
