#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "CCPaintedFigure.h"

#if (CC_TARGET_PLATFORM != CC_PLATFORM_BLACKBERRY)
#include "CCEditBox.h"
#endif

class ScoreBoardLayer : public cocos2d::CCLayerColor
{
public:
	ScoreBoardLayer();
	virtual ~ScoreBoardLayer();
    
	virtual bool init();
    
    CREATE_FUNC(ScoreBoardLayer);
    
	virtual void manuPrivatePressed(cocos2d::CCObject* pSender);
	virtual void manuTotalPressed(cocos2d::CCObject* pSender);
	virtual void menuDailyPressed(cocos2d::CCObject* pSender);
    
    virtual void setColor(const cocos2d::ccColor3B& color3);
    
protected:
    void updateRankingTable(CCNode *node, void *vector);
    
    cocos2d::CCMenuItemSprite *_privateItem;
    cocos2d::CCMenuItemSprite *_totalItem;
    cocos2d::CCMenuItemSprite *_dailyItem;
    
    cocos2d::CCArray *_tableRecords;
};

class MainMenuScene : public cocos2d::CCLayerColor
{
public:
	MainMenuScene();
	~MainMenuScene();
    
	// Here's a difference. Method 'init' in cocos2d-x returns bool,
    // instead of returning 'id' in cocos2d-iphone
	virtual bool init();
    
	// there's no 'id' in cpp, so we recommand to return the exactly class pointer
	static cocos2d::CCScene* scene();
    
	virtual void menuStartGameCallback(cocos2d::CCObject* pSender);
	virtual void menuShowScoreCallback(cocos2d::CCObject* pSender);
	virtual void menuChangeColorCallback(cocos2d::CCObject* pSender);
    
	// implement the "static node()" method manually
	CREATE_FUNC(MainMenuScene);
    
	void registerWithTouchDispatcher();
    
protected:
	cocos2d::CCArray *_colorMenuItems;
    cocos2d::CCArray *_colorThemeItems;
    
    cocos2d::CCLayerColor *_themeColorLayer;
    cocos2d::CCLabelTTF *_rankingLabel;
    cocos2d::CCLabelTTF *_playerLabel;
    
#if (CC_TARGET_PLATFORM != CC_PLATFORM_BLACKBERRY)
    cocos2d::extension::CCEditBox *_playerEdit;
#endif
    
    cocos2d::CCSprite* _normalScore;
    cocos2d::CCSprite* _pressedScore;
    
    ScoreBoardLayer *_scoreBoard;
};

#endif  // __HELLOWORLD_SCENE_H__