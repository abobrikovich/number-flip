//
//  CCRandomGenerator.h
//  FastNumbers
//
//  Created by CAMOBAP on 12/20/13.
//
//

#ifndef __FastNumbers__CCRandomGenerator__
#define __FastNumbers__CCRandomGenerator__

#include <vector>
#include <sstream>

class CCRandUniqueGeneratorFromRange
{
public:
    virtual ~CCRandUniqueGeneratorFromRange() {}
    virtual int getNextValue();
    
    static CCRandUniqueGeneratorFromRange* getFairGenerator(int max)
    {
        return new CCRandUniqueGeneratorFromRange(max);
    }
    static CCRandUniqueGeneratorFromRange* getFairGenerator(int start, int count)
    {
        return new CCRandUniqueGeneratorFromRange(start, count);
    }
    
private:
    CCRandUniqueGeneratorFromRange(int max);
    CCRandUniqueGeneratorFromRange(int start, int count);
    CCRandUniqueGeneratorFromRange(const std::vector<int> & values);
    
    std::vector<int> _values;
};

#endif /* defined(__FastNumbers__CCRandomGenerator__) */
