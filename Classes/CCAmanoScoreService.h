//
//  CCAmanoScoreService.h
//  FastNumbers
//
//  Created by CAMOBAP on 1/18/14.
//
//

#ifndef __FastNumbers__CCAmanoScoreService__
#define __FastNumbers__CCAmanoScoreService__

#include "cocos2d.h"
#include "CCCommon.h"

#include "HttpClient.h"

#include <vector>

NS_CC_EXTENSION_BEGIN

class ScoreRow : public cocos2d::CCObject
{
public:
    ScoreRow(const char *name, const double score) : playerName(name), score(score) {}
    
    const char * playerName;
    const double score;
};

class CCAmanoScoreService : public cocos2d::CCObject
{
public:
    virtual ~CCAmanoScoreService();
    static CCAmanoScoreService* sharedService(void);
    
    void init();
    
    std::string currentUsername();
    void updateUsername(const char *playerName);
    void addNewResult(const float score);
    void ranking(const char *type, CCNode *target, SEL_CallFuncND func, int count = 10, int offset = 0);
    
protected:
    void createUser();
    void updateAppLevelHash(const char *date, char * hash);
    void updateUserLevelHash(const char *date, char * hash);
    
    void onHttpRequestCompleted(CCHttpClient *sender, CCHttpResponse *response);
    
private:
    CCAmanoScoreService(void);
    
    std::vector<ScoreRow> _daily_scores;
    std::vector<ScoreRow> _total_scores;
    std::vector<ScoreRow> _private_scores;
    
    int _user_id;
    std::string _user_key;
    std::string _user_name;
    
    SEL_CallFuncND _score_callback;
    CCNode *_callback_target;
};

NS_CC_EXTENSION_END

#endif /* defined(__FastNumbers__CCAmanoScoreService__) */
