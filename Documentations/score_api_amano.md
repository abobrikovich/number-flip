# Score API for Number Flip

The main features are as follows:

Contents

1) Authorization
2) Create a user
3) Update a username
4) Post a score
5) Get rankings

1) authorization
To prevent unauthorized registration, make a simple authentication with hash.
App auth and user auth are used for different APIs.

1a) App auth
Include the parameters on the basis of the key string below to generate a hash value by `SHA-256`, and transmits.

	[app_id] + [timestamp] + [app_key]
	
timestamp format is
[yyyy-MM-dd HH:mm:ss]
	
Number Flip's app_id and app_key are as follows:

|app_id | app_key                         |
|-------|---------------------------------|
|2      | 63624b075719a5c53388b00dff787920|

1b) User auth
Please include the parameters on the basis of the key string below to generate a hash value by `SHA-256`, and transmits.

	[user_id] + [app_id] + [timestamp] + [user_key] + [app_key]
	
user_id and user_key are returned when you create a user.
So please create user during initial startup and save them.
## HOST
|environment | URL                              | status |
|------------|-----------------------------------|-------|
|sandbox     | http://appscore.sb.hautecouture.jp|enable|
|production  |http://appscore.hautecouture.jp    |ready|

2) Create a user

2a) post
required authorization: app auth

	POST /users
	{
		app_id: [app_id],
		timestamp: [yyyy-MM-dd HH:mm:ss],
		hash: [xxxxxxxxxxxxxxxxxxxxxx]
	}

2b) response
	status: 201 created
	body:
	{
		status: 1,
		message: 'success',
		user: {
			id: [user_id],
			name: [user name],
			key: [user key]
		}
	}
	---
	status: 403 forbidden
	body:
	{ 
		status: 3,
		message: 'invalid app'
	}
	---
	status: bad_request
	body:
	{
		status: 2,
		message: [error messages]
	}


3) Update a username

3a) post 
required authorization: user auth

	PUT /users/[user_id]
	{
		user_id: [user_id],
		app_id: [app_id],
		timestamp: [yyyy-MM-dd HH:mm:ss],
		hash: [xxxxxxxxxxxxxxxxxxxxxx],
		name: [new name]
	}

3b) response
	status: 201 created
	body:
	{
		status: 1,
		message: 'success'
	}
	---
	status: 403 forbidden
	body:
	{ 
		status: 3,
		message: 'invalid app'
	}
	---
	status: bad_request
	body:
	{
		status: 2,
		message: [error messages]
	}

4) Post score

4a) post
required authrization: user auth

	POST /scores
	{
		user_id: [user_id],
		app_id: [app_id],
		timestamp: [yyyy-MM-dd HH:mm:ss],
		hash: [xxxxxxxxxxxxxxxxxxxxxx],
		score: [score value](int)
	}

4b) response
	status: 201 created
	body:
	{
		status: 1,
		message: 'success'
	}
	---
	status: 403 forbidden
	body:
	{ 
		status: 3,
		message: 'invalid app'
	}
	---
	status: bad_request
	body:
	{
		status: 2,
		message: [error messages]
	}

5) Get rankings

5a) post send
required authorization: app auth
	
	GET /ranking/total
	{
		app_id: [app_id],
		timestamp: [yyyy-MM-dd HH:mm:ss],
		hash: [xxxxxxxxxxxxxxxxxxxxxx],
		count: [get count](int),  // default: 20, not need necessary
		offset: [offset](int) // default: 0, not need necessary
	}

5b) response
	status: 200 OK
	body:
	{
		status: 1,
		ranking:
		[
			{
				id: [score_id],
				name: [user_name],
				rank: [rank],
				score: [score value](int)
			},
			{
			...
			}
		]
	}
	---
	status: 403 forbidden
	body:
	{ 
		status: 3,
		message: 'invalid app'
	}


Update:

Yes, I have implemented next ranking APIs: 
[public rankings]
daily ranking: /ranking/daily
weekly ranking: /ranking/weekly
monthly ranking: /ranking/monthly
total ranking: /ranking/total

[private rankings]
daily ranking: /ranking/user/daily
weekly ranking: /ranking/user/weekly
monthly ranking: /ranking/user/monthly
total ranking: /ranking/user/total