LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE_FILENAME := libnumberflip
LOCAL_MODULE := numberflip_shared

COCOS2DX_ROOT := /Volumes/Data/Developers/Library/cocos2d-x-2.2.1
#$(COCOS2DX_ROOT)

LOCAL_SRC_FILES := numberflip/main.cpp \
                   ../../Classes/AppDelegate.cpp \
                   ../../Classes/MainMenuScene.cpp \
                   ../../Classes/GameScene.cpp \
                   ../../Classes/CCCommon.cpp \
                   ../../Classes/CCPaintedFigure.cpp \
                   ../../Classes/CCPersistentManager.cpp \
                   ../../Classes/CCRandomGenerator.cpp \
                   ../../Classes/CCAmanoScoreService.cpp \
                   ../../Classes/DataDirector.cpp
                   
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes \
                    $(LOCAL_PATH)/../../ThirdParty/android/prebuilt/libssl/include \
                    $(LOCAL_PATH)/../../ThirdParty/android/prebuilt/libcrypto/include \
                    $(COCOS2DX_ROOT)/extensions \
                    $(COCOS2DX_ROOT)/extensions/LocalStorage \
                    $(COCOS2DX_ROOT)/extensions/GUI/CCEditBox \
                    $(COCOS2DX_ROOT)/extensions/GUI/CCControlExtension \
                    $(COCOS2DX_ROOT)/extensions/network \
                    /Volumes/Data/Developers/Library/rapidjson/include

LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static cocos_extension_static cocosdenshion_static 
            
include $(BUILD_SHARED_LIBRARY)

# TODO FIND WAY TO REPLACE THIS WITH $(COCOS2DX_ROOT)
# 

$(call import-add-path, $(COCOS2DX_ROOT))
$(call import-add-path, $(COCOS2DX_ROOT)/cocos2dx/platform/third_party/android/prebuilt)
$(call import-add-path, $(LOCAL_PATH)/../../ThirdParty/android/prebuilt)

$(call import-module,cocos2dx)
$(call import-module,extensions)
$(call import-module,CocosDenshion/android)

$(call import-module,libssl)
$(call import-module,libcrypto)
